<?php

?>
<html>
    
<head>
    <?php require_once(__DIR__.'/bootstrap/head.php'); ?>
    <title>Find</title>
</head>
    
<body>
<?php require_once(__DIR__.'/partial/navigation.php'); ?>
<div class="container">
<?php
$search = $_POST['search'];

require_once(__DIR__.'/settings/config.php');
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT DISTINCT id, name, surname, number, img FROM contacts WHERE name LIKE '%".$search."%' OR surname LIKE '%".$search."%' OR number LIKE '%".$search."%';");

    $stmt->execute();

    foreach($stmt->fetchAll() as $k=>$v){
        echo "<div class='box col-md-2'><img src='/static/".$v['img']."' class='img-thumbnail'><address class='text-justify'><br><strong>Name: </strong>" .$v['name']."<br><strong>Surname: </strong>".$v['surname']."<br><strong>Number: </strong>".$v['number']."</address>
        <address><form class='inline' action='/modify.php' method='post'><input type='hidden' name='id' value='".$v["id"]."'><input type='submit' value='Modify' class='btn btn-default'></form><form class='inline2' action='/delete.php' method='post'><input type='hidden' name='id' value='".$v["id"]."'><input type='submit' value='Delete' class='btn btn-default'></form></address></div>";
    }

    }
  
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }
$conn = null;
#}

?>
</div>
</body>

</html>
