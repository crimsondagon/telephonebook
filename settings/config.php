<?php
#mysql-ctl cli
#$servername = $IP;
#$username = "tvergiles";
#$password = "";
#$db = "telephonebook";
#$port = 3306;

#$conn = new mysqli($servername, $username, $password, $db);
$servername = $IP;
$username = "";
$password = "";
$dbname = "telephonebook";

function connectdb() {

try {
    $conn = new PDO("mysql:host=$servername;dbname=telephonebook", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
}
?>