<?php
#require_once(__DIR__.'/settings/config.php');
$current = "add";
?>
<html>
    
<head>
    <?php require_once(__DIR__.'/bootstrap/head.php'); ?>
    <title>New</title>
</head>
    
<body>
<?php require_once(__DIR__.'/partial/navigation.php'); ?>
<div class="container">

    
<form role="form" method="post" action="new.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="email">Name:</label>
    <input type="text" class="form-control"  name="name">
  </div>
  <div class="form-group">
    <label for="pwd">Surname:</label>
    <input type="text" class="form-control" name="surname">
  </div>
  <div class="form-group">
    <label for="pwd">Number:</label>
    <input type="text" class="form-control" name="number">
  </div>
  <div class="form-group">
    <label for="pwd">Image:</label>
  <input type="hidden" name="MAX_FILE_SIZE" value="307777000" />
  <input type="file" name="userfile" id="fileToUpload">
  </div>
  <button type="submit" class="btn btn-default">Add</button>
</form>     
    
<?php

$name = $_POST['name'];
$surname = $_POST['surname'];
$number = $_POST['number'];
$uploaddir = __DIR__.'/static/';
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

if((!empty($surname) && !empty($name)) && !empty($number)) {

require_once(__DIR__.'/settings/config.php');
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stmt = $conn->prepare("INSERT INTO contacts (name, surname, number, img)
    VALUES (:name, :surname, :number, :img)");
    
    $stmt->bindParam(':name', $prepname);
    $stmt->bindParam(':surname', $prepsurname);
    $stmt->bindParam(':number', $prepnumber);
    $stmt->bindParam(':img', $prepimg);
     
    if(!empty($_FILES["userfile"]["name"])){
    $temp = explode(".", $_FILES["userfile"]["name"]);
    #$newfilename = round(microtime(true)) . '.' . end($temp);
    $newfilename = $name.$surname.$number. '.'. end($temp);
    move_uploaded_file($_FILES["userfile"]["tmp_name"], $uploaddir . $newfilename);
    $imgname = $newfilename;
    #if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    #    echo "File is valid, and was successfully uploaded.\n";
    #} else {
    #     echo "Possible file upload attack!\n";
    #}
    $prepimg = $imgname;
    }else{
    $prepimg = "default.jpg";
    }
    $prepname = $name;
    $prepsurname = $surname;
    $prepnumber = $number;
    
    $stmt->execute();
    echo "<div class='panel panel-default'>
        <div class='panel-heading'>Contact added</div>
        </div>";
    }
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }

$conn = null;
}else{
  echo "<div class='panel panel-default'>
        <div class='panel-heading'>Please enter data into all fields</div>
        </div>";
}
?>
</div> 
</body>

</html>
