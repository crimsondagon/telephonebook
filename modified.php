<?php
#require_once(__DIR__.'/settings/config.php');
?>
<html>
    
<head>
    <?php require_once(__DIR__.'/bootstrap/head.php'); ?>
    <title>New</title>
</head>
    
<body>
<?php require_once(__DIR__.'/partial/navigation.php'); ?>
<div class="container">
    

<?php

require_once(__DIR__.'/settings/config.php');


try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stmt = $conn->prepare("UPDATE contacts SET name = :name, surname = :surname, number = :number, img = :img WHERE id = :id");
    
    $stmt->bindParam(':name', $prepname);
    $stmt->bindParam(':surname', $prepsurname);
    $stmt->bindParam(':number', $prepnumber);
    $stmt->bindParam(':id', $prepid);
    $stmt->bindParam(':img', $prepnewimg);
    
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $number = $_POST['number'];
    $id = $_POST['id'];
    $oldimg = $_POST['oldimg'];
    $removeimg = $_POST['remove'];
    if($removeimg == "y"){
        $prepnewimg = "default.jpg";
        if($oldimg != "default.jpg"){
        unlink(__DIR__.'/static/'.$oldimg);
        }
    }elseif(!empty($_FILES["userfile"]["name"])){
        if($oldimg != "default.jpg") {
            $delimg = $oldimg;
            unlink(__DIR__.'/static/'.$delimg);
        }
    $uploaddir = __DIR__.'/static/';
    $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
    $temp = explode(".", $_FILES["userfile"]["name"]);
    $newfilename = $name.$surname.$number. '.'. end($temp);
    move_uploaded_file($_FILES["userfile"]["tmp_name"], $uploaddir . $newfilename);
    $prepnewimg = $newfilename;
    $imgname = $newfilename;
    }else{
    $prepnewimg = $oldimg;
    }
    
    
    $prepid = $id;
    $prepname = $name;
    $prepsurname = $surname;
    $prepnumber = $number;
    
    $stmt->execute();
    echo "<div class='panel panel-default'>
        <div class='panel-heading'>Contact modified</div>
        </div>";
    }
    
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }
    
$conn = null;
    

?>
</div> 
</body>

</html>
